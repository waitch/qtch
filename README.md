# qtch服务器框架

作为一个服务器框架，为开发人员更好的开发服务器

## 已经实现的功能

1. 日志模块
2. 配置模块
3. 协程模块
4. 协程调度模块
5. IoManger模块
6. socket模块
7. hook模块
8. tcpServer模块
9. httpServer模块

## 待实现功能（想法）

- [x] 日志模块根据日志等级输出不同颜色，并且可以配置
- [x] 完成orm模块
- [x] orm模块判断文件是否更新，如果没有更新就不生成
- [x] 完成http_connect模块
- [ ] 完成mysql模块
- [x] 完成sqlite模块
- [x] 完成postgresql模块
- [ ] 实现rtmp服务器
- [ ] 实现rtsp服务器
- [ ] 连接Redis模块
- [x] 完成email模块
- [x] 实现邮件发送功能
- [ ] 实现企业微信发送消息
- [x] 实现启动时配置线程数量
- [ ] 实现运行时配置线程数量

## 依赖的库

- yaml-cpp
- libqp
- boost
- openssl
- ragel
- jsoncpp

## 日志配置

> 日志配置

``` yaml
logs:
    - name: root
      level: debug
      appenders:
          - type: FileLogAppender
            level: debug
            file: /home/qiu/ueim-http-server/logs/root.txt
          - type: StdoutLogAppender
            level: info
            color:
              debug: default
              info: default
              warn: yellow
              error: red
              fatal: red
```

> 日志颜色

- default
- blue
- green
- red
- yellow
- white
- cyan
- magenta
- brightBlue
- brightGreen
- brightRed
- brightYellow
- brightWhite
- brightCyan
- brightMagenta

## 数据库支持

> 目前支持的数据库

- postgresql
- sqlite

### sqlite

> 注意事项

1. execute 和 query 一次只能执行一个语句，如果有多个语句，将只会执行第一个语句