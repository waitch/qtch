#ifndef QTCH_THREAD_H__
#define QTCH_THREAD_H__

#include<memory>
#include<pthread.h>
#include<functional>

#include"noncopyable.h"
#include"mutex.h"


namespace qtch{

class Thread : Noncopyable{
public:
    typedef std::shared_ptr<Thread> ptr;
    Thread(std::function<void()> cb,std::string name);
    static void* run(void* arg);
    void join();
    static const std::string GetThreadName();
    static Thread * GetThis();
    static void SetName(const std::string& name);
    int getId() const{ return m_id;}


private:
    pid_t m_id = -1;
    pthread_t m_thread = 0;
    std::function<void()> m_cb;
    std::string m_name;
    Semaphore m_sem;

};




}



#endif