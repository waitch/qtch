#ifndef __QTCH_HTTP_CONNECTION_H__
#define __QTCH_HTTP_CONNECTION_H__

#include <memory>
#include "http.h"
#include "qtch/streams/socket_stream.h"
#include "qtch/uri.h"
#include "qtch/mutex.h"
#include "qtch/bytearray.h"

namespace qtch {
namespace http {


class HttpResult {
public:
    typedef std::shared_ptr<HttpResult> ptr;

#define ERROR_NAME(XX) \
    XX(0,   OK,                         no error) \
    XX(1,   INVALID_URL,                invalid url) \
    XX(2,   INVALID_HOST,               invalid host) \
    XX(3,   CONNECT_FAIL,               connect faile) \
    XX(4,   SEND_CLOSE_PEER,            The connection is closed by the other party) \
    XX(5,   SEND_SOCKET_ERROR,          Socket error caused by sending request) \
    XX(6,   TIMEOUT,                    timeout) \
    XX(7,   CREAT_SOCKET_ERROR,         create socket error) \
    XX(8,   POOL_GET_CONNECTION,        get connect from pool error) \
    XX(9,   POOL_INVALID_CONNECTION,    Invalid connection)

    enum class Error {
        #define XX(num, code, desc) code = num,
            ERROR_NAME(XX)
        #undef XX
    };

    static const char* GetErrorString(Error error){
    #define XX(num,code,desc) \
        if(num == (int)error) {\
            return #desc; \
        }

        ERROR_NAME(XX);
        
    #undef XX    
        return "unknow error";
        
    }

    HttpResult(Error result, HttpResponse::ptr response,const std::string& error);

    Error m_result;
    HttpResponse::ptr m_resonse;
    std::string m_error;
    std::string toString() const;

};

class HttpConnectionPool;

class HttpConnection : public SocketStream {
friend HttpConnectionPool;
public:
    typedef std::shared_ptr<HttpConnection> ptr;

    static HttpResult::ptr DoGet(const std::string& url, uint64_t timeout_ms
                                , const std::map<std::string, std::string>& header = {}
                                , const std::string& body = "");
    static HttpResult::ptr DoPost(const std::string& url, uint64_t timeout_ms
                                , const std::map<std::string, std::string>& header = {}
                                , const std::string& body = "");
    static HttpResult::ptr DoRequest(HttpMethod method, const std::string& url, uint64_t timeout_ms
                                , const std::map<std::string, std::string>& header = {}
                                , const std::string& body = "");
    static HttpResult::ptr DoRequest(HttpRequest::ptr req, const std::string& url, uint64_t timeout_ms);

    static HttpResult::ptr DoGet(Uri::ptr uri, uint64_t timeout_ms
                                , const std::map<std::string, std::string>& header = {}
                                , const std::string& body = "");
    static HttpResult::ptr DoPost(Uri::ptr uri, uint64_t timeout_ms
                                , const std::map<std::string, std::string>& header = {}
                                , const std::string& body = "");
    static HttpResult::ptr DoRequest(HttpMethod method, Uri::ptr uri, uint64_t timeout_ms
                                , const std::map<std::string, std::string>& header = {}
                                , const std::string& body = "");
    static HttpResult::ptr DoRequest(HttpRequest::ptr req, Uri::ptr uri, uint64_t timeout_ms);

    HttpConnection(Socket::ptr sock, bool owner = true);

    virtual ~HttpConnection();
    HttpResponse::ptr recvResponse();
    int sendRequest(HttpRequest::ptr req);

private:
    uint64_t m_createTime = 0;
    uint64_t m_request = 0;
    ByteArray::ptr m_ba;
};

class HttpConnectionPool {
public:
    typedef std::shared_ptr<HttpConnectionPool> ptr;
    typedef Mutex MutexType; 

    /**
     * @brief 创建一个http请求池
     * 
     * @param uri http请求域名，带https或https
     * @param v_host 虚拟域名，host头使用
     * @param max_size 请求池大小
     * @param max_alive_time 连接最大时间，ms单位
     * @param max_request 每个连接最多请求次数
     * @return HttpConnectionPool::ptr 
     */
    static HttpConnectionPool::ptr Create(const std::string& uri
                                        , const std::string& v_host
                                        , uint32_t max_size
                                        , uint32_t max_alive_time
                                        , uint32_t max_request);
    HttpConnectionPool(const std::string& host
                    , const std::string& vhost
                    , uint32_t port
                    , bool isHttps
                    , uint32_t max_size
                    , uint32_t max_alive_time
                    , uint32_t max_request);
    
    HttpConnection::ptr getConnection();

    HttpResult::ptr DoGet(const std::string& url, uint64_t timeout_ms
                                , const std::map<std::string, std::string>& header = {}
                                , const std::string& body = "");
    HttpResult::ptr DoPost(const std::string& url, uint64_t timeout_ms
                                , const std::map<std::string, std::string>& header = {}
                                , const std::string& body = "");
    HttpResult::ptr DoRequest(HttpMethod method, const std::string& url, uint64_t timeout_ms
                                , const std::map<std::string, std::string>& header = {}
                                , const std::string& body = "");
    HttpResult::ptr DoRequest(HttpRequest::ptr req, const std::string& url, uint64_t timeout_ms);

    HttpResult::ptr DoGet(Uri::ptr uri, uint64_t timeout_ms
                                , const std::map<std::string, std::string>& header = {}
                                , const std::string& body = "");
    HttpResult::ptr DoPost(Uri::ptr uri, uint64_t timeout_ms
                                , const std::map<std::string, std::string>& header = {}
                                , const std::string& body = "");
    HttpResult::ptr DoRequest(HttpMethod method, Uri::ptr uri, uint64_t timeout_ms
                                , const std::map<std::string, std::string>& header = {}
                                , const std::string& body = "");
    HttpResult::ptr DoRequest(HttpRequest::ptr req, Uri::ptr uri, uint64_t timeout_ms);


private:
    static void ReleasePtr(HttpConnection* conn_ptr, HttpConnectionPool* pool);


private:
    std::string m_host;
    std::string m_vhost;
    uint32_t m_port;
    uint32_t m_maxSize;
    uint32_t m_maxAliveTime;
    uint32_t m_maxRequest;
    bool m_isHttps;

    MutexType m_mutex;
    std::list<HttpConnection*> m_conns;
    std::atomic<int32_t> m_total = {0};

};


}
}





#endif