#ifndef __QTCH_HTTP_SERVER_H__
#define __QTCH_HTTP_SERVER_H__

#include "qtch/tcp_server.h"
#include "http_session.h"
#include "servlet.h"

namespace qtch {
namespace http{

class HttpServer : public TcpServer {
public:
    typedef std::shared_ptr<HttpServer> ptr;
    HttpServer(bool keeplive = false
              ,qtch::IOManager* worker = IOManager::getThis()
              ,qtch::IOManager* ioWorker = IOManager::getThis()
              ,qtch::IOManager* acceptWorker = IOManager::getThis());
    ServletDispath::ptr getServletdispatch() const {return m_dispatch;}
    void setServletdispatch(ServletDispath::ptr v) { m_dispatch = v;}

protected:
    virtual void handleClient(Socket::ptr client) override;

private:
    bool m_isKeeplive;
    ServletDispath::ptr m_dispatch;

};




}
}



#endif