#ifndef __QTCH_HTTP_SESSION_H__
#define __QTCH_HTTP_SESSION_H__

#include "qtch/streams/socket_stream.h"
#include "http.h"
#include "qtch/bytearray.h"

namespace qtch{
namespace http{

class HttpSession : public SocketStream {
public:
    typedef std::shared_ptr<HttpSession> ptr;
    HttpSession(Socket::ptr sock, bool owner = true, size_t byteBaseSize = 4096);

    HttpRequest::ptr recvRequest();
    int sendResponse(HttpResponse::ptr rsp);

private:
    ByteArray::ptr m_ba;
};


}
}


#endif