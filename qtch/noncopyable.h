#ifndef __QTCH_NONCOPYABLE_H__
#define __QTCH_NONCOPYABLE_H__

namespace qtch{


class Noncopyable{
public:
    Noncopyable(){};
    ~Noncopyable(){};
    Noncopyable(const Noncopyable& copy) = delete;
    Noncopyable operator = (const Noncopyable& copy) = delete;
};

}




#endif