#include"mutex.h"
#include"log.h"

namespace qtch{

static Logger::ptr logger = QTCH_LOG_NAME("system");

Semaphore::Semaphore(){
    sem_init(&m_sem,0,0);
}

Semaphore::~Semaphore(){
    sem_destroy(&m_sem);
}

void Semaphore::wait(){
    sem_wait(&m_sem);
}

void Semaphore::notify(){
    sem_post(&m_sem);
}

RWMutex::RWMutex(){
    pthread_rwlock_init(&m_lock,nullptr);
}

RWMutex::~RWMutex(){
    pthread_rwlock_destroy(&m_lock);
}

void RWMutex::rlock(){
    pthread_rwlock_rdlock(&m_lock);
    // std::cout << "RWMutex::rlock" << std::endl;
}

void RWMutex::wlock(){
    pthread_rwlock_wrlock(&m_lock);
    // std::cout << "RWMutex::wlock" << std::endl;
}

void RWMutex::unlock(){
    pthread_rwlock_unlock(&m_lock);
    // std::cout << "RWMutex::unlock" << std::endl;
}

void NullMutex::unlock(){
    QTCH_LOG_DEBUG(logger) <<"unlock";
}

void NullMutex::lock(){
    QTCH_LOG_DEBUG(logger) <<"lock";
}

}