#ifndef __QTCH_MODULE_H__
#define __QTCH_MODULE_H__

#include "singleton.h"
#include "mutex.h"
#include <map>
#include <string>
#include <unordered_map>
#include <memory>
#include <vector>
#include <functional>

namespace qtch {

class Module{
public:
    typedef std::shared_ptr<Module> ptr;
    Module(const std::string& name,const std::string& version,const std::string& desc);
    virtual ~Module(){};
    virtual void onBeforeArgsParse(int argc,char** argv);
    virtual void onAfterArgsParse(int argc,char** argv);
    virtual bool onLoad();
    virtual bool onUnload();
    virtual bool onServerReady();
    virtual bool onServerUp();

    virtual std::string statusString();

    const std::string& getName() const {return m_name;}
    const std::string& getVersion() const {return m_version;}
    const std::string& getDesc() const {return m_desc;}
    const std::string& getFileName() const {return m_fileName;}
    void setFileName(const std::string& fileName) {m_fileName = fileName;}

private:
    std::string m_name;
    std::string m_version;
    std::string m_desc;
    std::string m_fileName;

};

class ModuleManager {
public:
    typedef RWMutex RWMutexType;
    typedef std::shared_ptr<ModuleManager> ptr;
    ModuleManager();

    void add(Module::ptr m);
    void del(const std::string& name);
    void delAll();

    void init();
    Module::ptr get(const std::string& name);
    void listAll(std::vector<Module::ptr>& m);
    void foreach(std::function<void(Module::ptr)> cb);

private:
    void InitModule(const std::string& path);
private:
    RWMutexType m_mutex;
    std::unordered_map<std::string,Module::ptr> m_modules;

};

typedef Singleton<ModuleManager> ModuleMgr;

}



#endif