#ifndef __QTCH_EMAIL_SMTP_H__
#define __QTCH_EMAIL_SMTP_H__
#include "qtch/email/email.h"
#include "qtch/streams/socket_stream.h"

namespace qtch {

class SmtpResult {
public:
    typedef std::shared_ptr<SmtpResult> ptr;
    enum class Result {
        IO_ERROR = -1,
        OK = 0,
    };
    SmtpResult(Result r, const std::string& m)
        :result(r)
        ,msg(m){
    }
    
    Result result;
    std::string msg;
};


class SmtpClient : public qtch::SocketStream {
public:
    typedef std::shared_ptr<SmtpClient> ptr;
    static SmtpClient::ptr Create(const std::string& host, uint32_t port, bool ssl = false);

    SmtpResult::ptr send(EMail::ptr email, int64_t timeout_ms = 1000, bool debug = false);

    std::string getDebugInfo();

protected:
    SmtpResult::ptr doCmd(const std::string& cmd, bool debug);

private:
    SmtpClient(Socket::ptr sock);
private:
    std::string m_host;
    std::stringstream m_ss;
    bool m_authed = false;
};


}


#endif