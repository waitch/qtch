#ifndef __QTCH_MACRO_H__
#define __QTCH_MACRO_H__

#include <assert.h>

#include "log.h"



#define QTCH_ASSERT(x) \
    if(!(x)) { \
        QTCH_LOG_ERROR(QTCH_LOG_ROOT()) << "ASSERTION: " #x \
            << "\nbacktrace:\n" \
            << qtch::BacktraceToString(100,2,"      "); \
        assert(x); \
    }


#define QTCH_ASSERT2(x,w) \
    if(!(x)) { \
        QTCH_LOG_ERROR(QTCH_LOG_ROOT()) << "ASSERTION: " #x \
            << "\n" << w \
            << "\nbacktrace:\n" \
            << qtch::BacktraceToString(100,2,"      "); \
        assert(x); \
    }

#endif