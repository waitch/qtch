#ifndef __QTCH_ENV_H__
#define __QTCH_ENV_H__

#include <memory>
#include <string>
#include <map>
#include <vector>
#include "mutex.h"
#include "singleton.h"


namespace qtch {

class Env {
public:
    typedef std::shared_ptr<Env> ptr;
    typedef RWMutex RWMutexType;

    bool init(int argc,char** argv);
    void add(const std::string& key, const std::string& val);
    bool has(const std::string& key);
    void del(const std::string& key);
    std::string get(const std::string& key, const std::string& default_val = "");

    void addHelp(const std::string& key,const std::string& desc);
    bool removeHelp(const std::string& key);
    void printfHelp();

    const std::string& getExe() const {return m_exe;}
    const std::string& getCwd() const {return m_cwd;}
    const std::string& getProgram() const {return m_program;}

    const std::string getEnv(const std::string& key, const std::string& default_val = "");
    bool setEnv(const std::string& key, const std::string& val);

    std::string getAbsolutePath(const std::string& path) const;
    std::string getAbsoluteWorkPath(const std::string& path) const;

    std::string getConfigPath();




private:
    RWMutexType m_mutex;
    std::vector<std::pair<std::string,std::string> > m_help;
    std::map<std::string, std::string> m_args;
    std::string m_program;
    std::string m_exe = "";
    std::string m_cwd = "";

};

typedef Singleton<Env> EnvMgr;

}




#endif