#ifndef __QTCH_WORKER_H__
#define __QTCH_WORKER_H__

#include "scheduler.h"
#include "iomanager.h"
#include "log.h"
#include "singleton.h"


namespace qtch {

class WorkerManager {
public:
    WorkerManager();
    void add(Scheduler::ptr s);
    Scheduler::ptr get(const std::string& name);
    IOManager::ptr getAsIOManager(const std::string& name);

    template<class FiberOrCb>
    void schedule(const std::string& name, FiberOrCb fc,int thread = -1){
        auto s = get(name);
        if(s){
            s->schedule(fc,thread);
        }else{
            static qtch::Logger::ptr s_logger = QTCH_LOG_NAME("system");
            QTCH_LOG_ERROR(s_logger) << "schedule name=" << name << " not exists";
        }
    }

    template<class InputIterator>
    void schedule(const std::string& name, InputIterator begin, InputIterator end){
        auto s = get(name);
        if(s){
            s->schedule(begin, end);
        }else{
            static qtch::Logger::ptr s_logger = QTCH_LOG_NAME("system");
            QTCH_LOG_ERROR(s_logger) << "schedule name=" << name << " not exists";
        }
    }

    bool init();
    bool init(const std::map<std::string,std::map<std::string,std::string> >& v);
    void stop();

    bool isStoped() const {return m_stop;}
    std::ostream& dump(std::ostream& os);
    std::string toString();

    uint32_t getCount();

private:
    std::map<std::string, Scheduler::ptr> m_datas;
    bool m_stop;


};

typedef qtch::Singleton<WorkerManager> WorkerMgr;

}


#endif