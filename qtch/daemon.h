#ifndef __QTCH_DAEMON_H__
#define __QTCH_DAEMON_H__

#include <string>
#include <memory>
#include <functional>
#include "singleton.h"

namespace qtch {

struct ProcessInfo{
    pid_t parentPid = 0;
    pid_t mainPid = 0;
    uint64_t parentStartTime = 0;
    uint64_t mainStartTime = 0;
    uint32_t restartCount = 0;

    std::string toString();
};

typedef Singleton<ProcessInfo> ProcessInfoMgr;

int start_daemon(int argc,char** argv
        , std::function<int(int,char**)> main_cb, bool is_deamon);

}
















#endif