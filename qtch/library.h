#ifndef __QTCH_LIBRARY_H__
#define __QTCH_LIBRARY_H__

#include <string>
#include <memory>
#include <dlfcn.h>
#include "module.h"

namespace qtch {

class Library{
public:
    static Module::ptr LoadModule(const std::string& path);
};

}


#endif