#ifndef __QTCH_APPLICATION_H__
#define __QTCH_APPLICATION_H__

#include "tcp_server.h"
#include "log.h"
#include "iomanager.h"
#include <map>
#include <functional>


namespace qtch {

class Application{
public:

    Application();
    static Application* GetInstance() {return m_application;}
    bool init(int argc,char** argv);
    bool run();

    static void stop();
    static int stopRunning();

    bool getTcpServer(const std::string& type,std::vector<TcpServer::ptr>& svrs);
    void listAllTcpServer(std::map<std::string,std::vector<TcpServer::ptr> >& servers);

    IOManager::ptr getMainIOManager() const {return m_mainIOManager;}

    /**
     * @brief 添加-s参数后的命令
     * 
     * @param command_name 命令名称
     * @param fun int返回值的无参函数，返回值success 0, error 1
     */
    void addCommand(const std::string& command_name, std::function<int()> fun);

private:
    int main(int argc,char** argv);
    int run_fiber();
    
    /**
     * @brief 执行command 命令
     * 
     * @param command stop，restart...
     * @return int success 0, error 1
     */
    int exec_command(const std::string& command);

private:

    std::map<std::string,std::vector<TcpServer::ptr> > m_tcpServers;
    int m_argc = 0;
    char ** m_argv = nullptr;

    static Application* m_application;
    IOManager::ptr m_mainIOManager;
    std::map<std::string, std::function<int ()> > m_commands_list; 
};

}


#endif