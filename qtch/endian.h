#ifndef __QTCH_ENDIAN_H__
#define __QTCH_ENDIAN_H__

#include <byteswap.h>
#include <stdint.h>
#include <type_traits>

#define QTCH_LITTLE_ENDIAN 1
#define QTCH_BIG_ENDIAN 2

namespace qtch{


template<class T>
typename std::enable_if<sizeof(T)==sizeof(uint64_t),T>::type
byteswap(T v){
    return (T)bswap_64((uint64_t)v);
}

template<class T>
typename std::enable_if<sizeof(T)==sizeof(uint32_t),T>::type
byteswap(T v){
    return (T)bswap_32((uint32_t)v);
}

template<class T>
typename std::enable_if<sizeof(T)==sizeof(uint16_t),T>::type
byteswap(T v){
    return (T)bswap_16((uint16_t)v);
}

#if BYTE_ORDER == BIG_ENDIAN
#define QTCH_BYTE_ORDER QTCH_BIG_ENDIAN
#else
#define QTCH_BYTE_ORDER QTCH_LITTLE_ENDIAN
#endif

#if QTCH_BYTE_ORDER == QTCH_BIG_ENDIAN

template<class T>
T byteswapToLittleEndian(T t){
    return byteswap(t);
}

template<class T>
T byteswapToBigEndian(T t){
    return t;
}


#else

template<class T>
T byteswapToLittleEndian(T t){
    return t;
}

template<class T>
T byteswapToBigEndian(T t){
    return byteswap(t);
}

#endif







}




#endif