#ifndef __QTCH_FD_MANAGE_H__
#define __QTCH_FD_MANAGE_H__

#include <memory>
#include <vector>

#include "macro.h"

namespace qtch{

class FdCtx : public std::enable_shared_from_this<FdCtx>{
public:
    typedef std::shared_ptr<FdCtx> ptr;
    FdCtx(int fd);
    ~FdCtx();
    bool isInit() const{return m_isInit;}
    bool isSocket() const{return m_isSocket;}
    bool isClosed() const{return m_isClosed;}
    void setUserNonblock(bool v){m_userNonblock = v;}
    bool getUserNonblock() const {return m_userNonblock;}
    void setSysNonblock(bool v){m_sysNonblock = v;}
    bool getSysNonblock() const {return m_sysNonblock;}
    void setTimeout(int type,uint64_t v);
    uint64_t getTimeout(int type);

private:
    bool init();


private:
    bool m_isInit = false;
    bool m_isSocket = false;
    bool m_sysNonblock = false;
    bool m_userNonblock = false;
    bool m_isClosed = false;
    int m_fd;
    uint64_t m_recvTimeout = -1;
    uint64_t m_sendTimeout = -1;

};

class FdManager{
public:
    typedef RWMutex RWMutexType;

    FdManager();
    FdCtx::ptr get(int fd, bool auto_create = false);
    void del(int fd);

private:
    RWMutexType m_mutex;
    std::vector<FdCtx::ptr> m_datas;

};

typedef Singleton<FdManager> FdMgr;

}















#endif