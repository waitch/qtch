#ifndef __QTCH_ORM_UTIL_H__
#define __QTCH_ORM_UTIL_H__

#include <memory>
#include <string>


namespace qtch {
namespace orm {

std::string GetAsClassName(const std::string& v);

std::string GetAsMemberName(const std::string& colName);

std::string GetAsMemberNameNull(const std::string& colName);

std::string getAsGetFunName(const std::string& v);

std::string getAsSetFunName(const std::string& v);

std::string getAsIsFunName(const std::string& v);

std::string getAsSetNullFunName(const std::string& v);

std::string getAsDefeineMacro(const std::string& v);


}
}


#endif