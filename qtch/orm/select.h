#ifndef __QTCH_ORM_SELECT_H__
#define __QTCH_ORM_SELECT_H__

#include <memory>
#include <string>
#include "label.h"
#include "column.h"

namespace qtch {
namespace orm {

class Select {
public:
    typedef std::shared_ptr<Select> ptr;
    
    Select(std::shared_ptr<Table> table);
    bool init(const tinyxml2::XMLElement& node);

    std::string gen_inc();
    std::string gen_src();
    std::string gen_ParameterList();

    bool getIsResultList() const {return m_isResultList;}
    Column::Type getResultType() const {return m_resultType;}
    const std::string& getId() const { return m_id;}

private:
    std::shared_ptr<Table> m_table;
    std::vector<Label::ptr> m_label;
    bool m_isResultList = false;
    Column::Type m_resultType = Column::Type::TYPE_TABLE;
    std::string m_id;
    bool m_usingEntiry = false;

};


}
}


#endif