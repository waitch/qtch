#ifndef __QTCH_ORM_INSERT_H__
#define __QTCH_ORM_INSERT_H__

#include <memory>
#include <string>
#include "label.h"

namespace qtch {
namespace orm {

class Table;
class Insert {
public:
    typedef std::shared_ptr<Insert> ptr;

    Insert(std::shared_ptr<Table> table);
    bool init(const tinyxml2::XMLElement& node);

    std::string gen_inc();
    std::string gen_src();
    std::string gen_ParameterList();
    std::string gen_default_inc();
    std::string gen_default_src();

private:
    std::shared_ptr<Table> m_table;
    std::vector<Label::ptr> m_label;
    std::string m_id;
    bool m_usingEntiry = false;
};



}
}





#endif