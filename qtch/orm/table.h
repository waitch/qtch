#ifndef __QTCH_ORM_TABLE_H__
#define __QTCH_ORM_TABLE_H__

#include <vector>
#include <memory>
#include <string>
#include <ostream>
#include "column.h"
#include "index.h"
#include "util.h"
#include "select.h"
#include "delete.h"
#include "update.h"
#include "insert.h"

namespace qtch {
namespace orm {

class Column;

class Table : public std::enable_shared_from_this<Table>{
public:
    typedef std::shared_ptr<Table> ptr;

    const std::string& getName() const {return m_name;}
    const std::string& getNamespace() const {return m_namespace;}
    const std::string& getDesc() const {return m_desc;}
    const std::vector<Column::ptr>& getCols() const {return m_cols;}
    const std::vector<Index::ptr>& getIdxs() const {return m_idxs;}
    const std::vector<Select::ptr>& getSelects() const {return m_selsects;}
    const std::vector<Delete::ptr>& getDeletes() const {return m_deletes;}
    const std::vector<Update::ptr>& getUpdates() const {return m_updates;}
    const std::vector<Insert::ptr>& getInserts() const {return m_inserts;}
    const std::string& getClassName();
    const std::string& getClassDaoName();


    bool init(const tinyxml2::XMLElement& node);

    std::string toString() const;
    std::ostream& dump(std::ostream& os) const;

    std::string getFileName() const;
    void gen(const std::string& path);

private:
    void gen_inc(const std::string& path);
    void gen_src(const std::string& path);

    void gen_entity_inc(const std::string& path, std::ostream& os);
    void gen_entity_src(const std::string& path, std::ostream& os);

    void gen_dao_inc(const std::string& path, std::ostream& os);
    void gen_dao_src(const std::string& path, std::ostream& os);
    



private:
    std::string m_name;
    std::string m_namespace;
    std::string m_desc;
    std::string m_class_entire_name;
    std::string m_class_dao_name;
    std::string m_subfix = "_info";
    std::string m_dbClass = "qtch::IDB";
    std::string m_queryClass = "qtch::IDB";
    std::string m_updateClass = "qtch::IDB";
    std::vector<Column::ptr> m_cols;
    std::vector<Index::ptr> m_idxs;
    std::vector<Select::ptr> m_selsects;
    std::vector<Delete::ptr> m_deletes;
    std::vector<Update::ptr> m_updates;
    std::vector<Insert::ptr> m_inserts;

};

std::ostream& operator<<(std::ostream& os, const Table& table);
std::ostream& operator<<(std::ostream& os, const Table::ptr table);

}
}


#endif