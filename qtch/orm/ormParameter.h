#ifndef __QTCH_ORM_ORMPARAMETER_H__
#define __QTCH_ORM_ORMPARAMETER_H__

#include <memory>
#include <string>

#include "column.h"

namespace qtch {
namespace orm {

class OrmParameter {
public:
    typedef std::shared_ptr<OrmParameter> ptr;

    OrmParameter();
    
    // str = #{name,type=xx [,xx=yy]}
    bool init(const std::string& str);
    
    

    bool isInit() const {return m_isInit;}
    const std::string& getName() const {return m_name;}
    Column::Type getType() const { return m_type;} 
    void setType(Column::Type v) {m_type = v;}

private:
    bool initType(const std::string& name,const std::string& value);


private:
    bool m_isInit;
    Column::Type m_type;
    std::string m_name;
};



}
}



#endif