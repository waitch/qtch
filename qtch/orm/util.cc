#include "util.h"
#include "qtch/util.h"
#include <sstream>

namespace qtch {
namespace orm {

std::string GetAsClassName(const std::string& v) {
    auto vs = qtch::StringUtil::split(v, '_');
    std::stringstream ss;
    for(auto& i : vs){
        i[0] = toupper(i[0]);
        ss << i;
    }
    return ss.str();
}

std::string GetAsMemberName(const std::string& colName) {
    auto memberName = GetAsClassName(colName);
    memberName[0] = tolower(memberName[0]);
    return "m_" + memberName;
}

std::string GetAsMemberNameNull(const std::string& colName) {
    auto memberName = GetAsClassName(colName);
    memberName[0] = tolower(memberName[0]);
    return "b_m_" + memberName;
}

std::string getAsGetFunName(const std::string& v) {
    auto MemberName = GetAsClassName(v);
    return "get" + MemberName;
}

std::string getAsSetFunName(const std::string& v) {
    auto MemberName = GetAsClassName(v);
    return "set" + MemberName;
}

std::string getAsIsFunName(const std::string& v) {
    auto MemberName = GetAsClassName(v);
    return "isNull" + MemberName;
}

std::string getAsSetNullFunName(const std::string& v) {
    auto MemberName = GetAsClassName(v);
    return "setNull" + MemberName;
}

std::string getAsDefeineMacro(const std::string& v) {
    std::string tmp = qtch::StringUtil::replace(v,'.','_');
    for(size_t i = 0; i < tmp.length(); ++i){
        tmp[i] = toupper(tmp[i]);
    }
    return "__" + tmp + "__"; 
}




}
}