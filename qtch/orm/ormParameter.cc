#include "ormParameter.h"
#include "qtch/log.h"




namespace qtch {
namespace orm {

static Logger::ptr logger = QTCH_LOG_NAME("orm");

OrmParameter::OrmParameter(){
    m_isInit = false;
    m_type = Column::Type::TYPE_NULL;
}

bool OrmParameter::init(const std::string& str){
    QTCH_LOG_DEBUG(logger) << "OrmParameter init str=" << str;
    if(!StringUtil::regexMatch(str,"#{\\w+(,\\w+=\\w+)*}")){
        QTCH_LOG_ERROR(logger) << "It's not Parameter str=" << str;
        return false;
    }
    std::string _str = str.substr(2,str.size()-3);
    std::vector<std::string> nodes = StringUtil::split(_str,",");
    m_name = StringUtil::trim(nodes[0]);
    for(size_t i = 1; i < nodes.size(); ++i){
        std::vector<std::string> temps = StringUtil::split(nodes[i],"=");
        if(temps.size()!=2){
            QTCH_LOG_ERROR(logger) << "Parameter init faile str=" << str 
                        << " attrs=" << nodes[i];
            return false;
        }
        std::string name = StringUtil::trim(temps[0]);
        std::string value = StringUtil::trim(temps[1]);
        initType(name,value);
    }
    m_isInit = true;
    return true;
}

bool OrmParameter::initType(const std::string& name,const std::string& value){
    if(strcasecmp(name.c_str(),"type")){
        return false;
    }
    m_type = Column::ParseType(value);
    if(m_type==Column::Type::TYPE_NULL){
        QTCH_LOG_ERROR(logger) << "initType faile by unkown type value=" << value;
        return false;
    }
    return true;
}


}
}