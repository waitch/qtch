#ifndef __QTCH_ORM_LABEL_H__
#define __QTCH_ORM_LABEL_H__

#include <memory>
#include <string>
#include "qtch/tinyxml2.h"
#include <vector>
#include "column.h"
#include "ormParameter.h"



namespace qtch {
namespace orm {

class Table;

class Label {
public:
    typedef std::shared_ptr<Label> ptr;

    static Label::ptr Create(const tinyxml2::XMLNode& node);

    virtual ~Label(){}
    virtual bool init(const tinyxml2::XMLNode& node) = 0;
    virtual bool init(const std::string& str) = 0;
    virtual const std::vector<OrmParameter::ptr>& getBindVec() {return m_binds;}
    virtual const void addBind(OrmParameter::ptr param){
        m_binds.push_back(param);
    }
    virtual std::string gen(std::shared_ptr<Table> table) = 0;

private:
    std::vector<OrmParameter::ptr> m_binds;
};

class StringLabel : public Label {
public:
    virtual ~StringLabel(){}
    typedef std::shared_ptr<StringLabel> ptr;
    virtual bool init(const tinyxml2::XMLNode& node) override;
    virtual bool init(const std::string& str) override;
    virtual std::string gen(std::shared_ptr<Table> table) override;
private:
    std::string m_str;
};


}
}



#endif