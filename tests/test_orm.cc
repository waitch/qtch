#include "qtch/qtch.h"
#include "orm_out/uesoft/im/im_user_info.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");

void test_postgresql(){
    QTCH_LOG_INFO(logger) << "test_postgresql";
    qtch::PostgreSQL::ptr pq_ptr = qtch::PostgresqlMgr::GetInstance()->get("aliyun");
    if(!pq_ptr){
        QTCH_LOG_ERROR(logger) << "postgresql connect faile";
        return;
    }
    uesoft::im::ImUserInfo::ptr user1 = uesoft::im::ImUserInfoDao::SelectByid(95,pq_ptr);
    if(!user1){
        QTCH_LOG_ERROR(logger) << "SelectByid faile";
        return;
    }
    std::string create_at;
    QTCH_LOG_INFO(logger) << "im_user [id=" << user1->getId() << ", userName=" << user1->getUserName() 
            << ", git_id=" << user1->getGitId() << ",pwd=" << user1->getUserPassword() << " ,nickName=" << user1->getUserNick()
            << ", sex=" << user1->getSex() << ", tel=" << user1->getTel() << ", create_at=" 
            << (qtch::time_to_postgresql_timestamp(user1->getCreatedAt(),create_at)?create_at:"null") << " ]"; 
    std::vector<uesoft::im::ImUserInfo::ptr> userVec;
    // while(1){
        int rt = uesoft::im::ImUserInfoDao::SelectAllUser(userVec,pq_ptr);
        if(!rt){
            QTCH_LOG_ERROR(logger) << "SelectAllUser faile";
            return;
        }
        for(size_t i=0;i<userVec.size();++i){
            uesoft::im::ImUserInfo::ptr user = userVec[i];
            QTCH_LOG_INFO(logger) << "im_user [id=" << user->getId() << ", userName=" << user->getUserName() 
                << ", git_id=" << user->getGitId() << ", pwd=" << user->getUserPassword() << ", nickName=" << user->getUserNick()
                << ", sex=" << user->getSex() << ", tel=" << user->getTel() << ", create_at=" 
                << (qtch::time_to_postgresql_timestamp(user1->getCreatedAt(),create_at)?create_at:"null") << " ]"; 
        }
    // }
    

}

void test_time(){
    QTCH_LOG_INFO(logger) << "---";
}




int main(){
    qtch::Config::LoadConfigDir("/home/qiu/qtch/bin/conf");
    qtch::IOManager iom(1);
    iom.schedule(test_postgresql);
    //iom.addTimer(2,test_time,true);
    iom.start();
    //test_postgresql();
}