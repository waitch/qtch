#include "qtch/qtch.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");

void test_request(){
    QTCH_LOG_INFO(logger) << "test request";
    qtch::http::HttpRequest::ptr req(new qtch::http::HttpRequest());
    req->setBody("hello world!");
    req->setHeader("host","www.sylar.top");
    QTCH_LOG_INFO(logger) << req;
}

void test_response(){
    QTCH_LOG_INFO(logger) << "test response";
    qtch::http::HttpResponse::ptr rsp(new qtch::http::HttpResponse());
    rsp->setHeader("X-X","sylar");
    rsp->setBody("hello world!");
    rsp->setStatus((qtch::http::HttpStatus)400);
    rsp->setClose(false);
    rsp->setCookie("abd","111",time(NULL) + 3600,"/bogl","www.sylar.top",true);
    QTCH_LOG_INFO(logger) << rsp;
}

void test_session_data(){
    qtch::http::SessionData::ptr sessiondata(new qtch::http::SessionData());
    sessiondata->setId("11");
    qtch::http::SessionDataMgr::GetInstance()->add(sessiondata);
    sessiondata->setData<std::string>("test1","test1");

    qtch::http::SessionData::ptr s1 = qtch::http::SessionDataMgr::GetInstance()->get("11");
    if(!s1){
        QTCH_LOG_ERROR(logger) << "find sessionData faile";
        return;
    }
    QTCH_LOG_INFO(logger) << "test1=" << s1->getData<std::string>("test1","tt");
    sleep(6);
    qtch::http::SessionDataMgr::GetInstance()->check(7);
    s1 = qtch::http::SessionDataMgr::GetInstance()->get("11");
    if(!s1){
        QTCH_LOG_ERROR(logger) << "find sessionData faile";
        return;
    }
    QTCH_LOG_INFO(logger) << "test1=" << s1->getData<std::string>("test1","tt");
    sleep(8);
    qtch::http::SessionDataMgr::GetInstance()->check(7);
    s1 = qtch::http::SessionDataMgr::GetInstance()->get("11");
    if(!s1){
        QTCH_LOG_ERROR(logger) << "find sessionData faile";
        return;
    }
    QTCH_LOG_INFO(logger) << "test1=" << s1->getData<std::string>("test1","tt");
}

int main(){
    test_request();
    test_response();
    test_session_data();
}   