#include "qtch/qtch.h"


static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");



void test(){

#define XX(type, len, write_fun, read_fun, base_len) {\
    std::vector<type> vec;\
    for(int i=0; i < len; ++i){\
        vec.push_back(rand());\
    }\
    qtch::ByteArray::ptr ba(new qtch::ByteArray(base_len));\
    for(size_t i=0; i < vec.size(); ++i){\
        ba->write_fun(vec[i]);\
    }\
    for(size_t i=0;i<vec.size();++i){\
        type v =ba->read_fun();\
        QTCH_ASSERT(v == vec[i]);\
    }\
    QTCH_ASSERT(ba->getReadSize() == 0);\
    QTCH_LOG_INFO(logger) << #write_fun "/" #read_fun \
                    << "(" #type ") len=" << len\
                    << " base_len=" << base_len\
                    << " size=" << ba->getWritePosition();\
}
    srand(time(NULL));
    XX(int8_t, 100, writeFint8, readFint8,1);
    XX(uint8_t, 100, writeFuint8, readFuint8,1);
    XX(int16_t, 100, writeFint16, readFint16,1);
    XX(uint16_t, 100, writeFuint16, readFuint16,1);
    XX(int32_t, 100, writeFint32, readFint32,1);
    XX(uint32_t, 100, writeFuint32, readFuint32,1);
    XX(int64_t, 100, writeFint64, readFint64,1);
    XX(uint64_t, 100, writeFuint64, readFuint64,1);

    XX(int32_t, 100, writeInt32, readInt32,1);
    XX(uint32_t, 100, writeUint32, readUint32,1);
    XX(int64_t, 2, writeInt64, readInt64,1);
    XX(uint64_t, 100, writeUint64, readUint64,1);

#undef XX

    std::string ss = "testashdoauihfoieghvwoihjfcvxclsknvoiwshfeoiwnjhpoiwehvoinsd";
    qtch::ByteArray::ptr ba(new qtch::ByteArray(1));
    ba->writeStringWithoutLength(ss);
    ba->writeToFile("/home/qiu/qtch/bytearray.in");
    qtch::ByteArray::ptr ba1(new qtch::ByteArray(1));
    ba1->readFromFile("/home/qiu/qtch/bytearray.in");
    std::string ss1 = ba1->toString();
    QTCH_LOG_INFO(logger) << "ba1.tostring=" << ba1->toString()
                        << " ba.tostring=" << ba->toString();
    QTCH_ASSERT(ss1 == ss);

}

int main(){
    test();
}