#include"qtch/qtch.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");
qtch::Timer::ptr timer;
void test_timer(){
    qtch::IOManager::ptr iom(new qtch::IOManager(2));
    timer = iom->addTimer(1000,[](){
        static int i = 0;
        QTCH_LOG_INFO(logger) << "hello timer i=" <<i;
        if(++i == 3){
            QTCH_LOG_DEBUG(logger) << "------------------------";
            timer->reset(2000,true);
            QTCH_LOG_DEBUG(logger) << "------------------------";
        }
        if(i==20){
            timer->cancel();
        }
    },true);
    QTCH_LOG_DEBUG(logger) << "------------------------";
    iom->start();
}

int main(){
    test_timer();
}