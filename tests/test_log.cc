#include<iostream>
#include<time.h>
#include"qtch/qtch.h"


void test_log(){
    qtch::Logger::ptr logger = QTCH_LOG_ROOT();
    QTCH_LOG_DEBUG(logger)<<"test log stdout";
    qtch::LogAppender::ptr appender2(new qtch::FileLogAppender("./log.txt"));
    appender2->setLevel(qtch::LogLevel::DEBUG);
    logger->addAppender(appender2);
    qtch::LogAppender::ptr appender3(new qtch::FileLogAppender("./log/log.txt"));
    appender3->setLevel(qtch::LogLevel::DEBUG);
    logger->addAppender(appender3);
    QTCH_LOG_DEBUG(logger)<<"test log fileout";
    QTCH_LOG_INFO(logger)<<"test log info";
}



int main(int argc,char ** argv){
    std::cout<<"hello world!"<<std::endl;
    test_log();
}