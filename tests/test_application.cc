#include "qtch/qtch.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");


int main(int argc,char** argv){
    qtch::Application app;
    if(app.init(argc,argv)){
        return app.run();
    }
    return 0;
}