#include "qtch/qtch.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");

class TestModule : public qtch::Module{
public:
    TestModule():qtch::Module("test","1.0.0","test"){}

    ~TestModule(){}
    virtual void onBeforeArgsParse(int argc,char** argv) override {
        QTCH_LOG_INFO(logger) << "testModule onBeforeArgsParse";
    }

    virtual void onAfterArgsParse(int argc,char** argv) override {
        QTCH_LOG_INFO(logger) << "testModule onAfterArgsParse";
    }

    virtual bool onLoad() override {
        QTCH_LOG_INFO(logger) << "testModule onLoad";
        return true;
    }

    virtual bool onUnload() override {
        QTCH_LOG_INFO(logger) << "testModule onUnload";
        return true;
    }

    virtual bool onServerReady() override {
        QTCH_LOG_INFO(logger) << "testModule onServerReady";
        return true;
    }

    virtual bool onServerUp() override {
        QTCH_LOG_INFO(logger) << "testModule onServerUp";
        return true;
    }

    virtual std::string statusString() override {
        QTCH_LOG_INFO(logger) << "testModule statusString";
        return qtch::Module::statusString();
    }


};

extern "C" {
    qtch::Module* CreateModule(){
        QTCH_LOG_INFO(logger) << "testModule CreateModule";
        return new TestModule();
    }

    void DestoryModule(qtch::Module* m){
        QTCH_LOG_INFO(logger) << "testModule DestoryModule";
        delete m;
    }
}