#include "qtch/tinyxml2.h"
#include "qtch/qtch.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");
char  xml[] = "<root>\n"
"abc\n"
"<test1>\n"
"d\n"
"</test1>\n"
"efg\n"
"<test2/>\n"
"e\n"
"</root>\n";

void test_xml(){
    QTCH_LOG_INFO(logger) << "\n" << xml;
    qtch::tinyxml2::XMLDocument doc;
    int rt;
    if((rt=doc.Parse(xml,strlen(xml)))){
        QTCH_LOG_ERROR(logger) << "error rt=" << rt <<" errstr=" << doc.ErrorStr();;
        return;
    }
    qtch::tinyxml2::XMLElement* root = doc.RootElement();
    qtch::tinyxml2::XMLNode* node =root->FirstChild();
    while(node){
        QTCH_LOG_INFO(logger) << "find value=" << node->Value();
        if(!node->ToElement()){
            QTCH_LOG_INFO(logger) << "node";
        }
        
        node = node->NextSibling();
    }
    
    // QTCH_LOG_INFO(logger) << root->

}

int main() {
    test_xml();
}