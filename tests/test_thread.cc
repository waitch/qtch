#include"qtch/qtch.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");

int count = 0;

qtch::RWMutex mutex;

void thread_run(){
    for(int i = 0; i < 100; ++i){
        QTCH_LOG_INFO(logger) << "thread run "<<i;
    }
}

void test_thread(bool flag){
    QTCH_LOG_INFO(logger) << "begin test_thread";
    qtch::Thread::ptr t1(new qtch::Thread(thread_run,"thread_1"));
    qtch::Thread::ptr t2(new qtch::Thread(thread_run,"thread_2"));
    qtch::Thread::ptr t3(new qtch::Thread(thread_run,"thread_3"));
    qtch::Thread::ptr t4(new qtch::Thread(thread_run,"thread_4"));
    qtch::Thread::ptr t5(new qtch::Thread(thread_run,"thread_5"));
    if(flag){
        t1->join();
        t2->join();
        t3->join();
        t4->join();
        t5->join();
    }
    QTCH_LOG_INFO(logger) << "end test_thread";

}

void mutex_run(){
    for(int i=0;i<100000;i++){
        qtch::RWMutex::WriteLock lock(mutex);
        count++;
    }
}

void test_Mutex(){
    qtch::Thread::ptr t1(new qtch::Thread(mutex_run,"thread_1"));
    qtch::Thread::ptr t2(new qtch::Thread(mutex_run,"thread_2"));
    qtch::Thread::ptr t3(new qtch::Thread(mutex_run,"thread_3"));
    t1->join();
    t2->join();
    t3->join();
    QTCH_LOG_INFO(logger) << "count:"<<count;
}

int main(){
    qtch::Config::LoadConfigDir("/home/qiu/qtch/bin/conf");
    //test_thread(true);
    test_Mutex();

}