#include "qtch/log.h"

static qtch::Logger::ptr logger = QTCH_LOG_ROOT();


void test_logFilePath(){
    qtch::FileLogAppender::ptr x = 
        qtch::FileLogAppender::ptr(new qtch::FileLogAppender("/home/qiu/UEIMHttpServer/logs/%d{%Y-%m-%d}/root_%d{%Y-%m-%d_%H_%M_%S}.txt"));
    
    QTCH_LOG_INFO(logger) << "fileName:" << x->getFileName();
}

int main(){
    test_logFilePath();
}