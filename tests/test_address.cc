#include "qtch/qtch.h"
#include<vector>



static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");


void test_mask(){

    QTCH_LOG_INFO(logger) << "QTCH_BYTE_ORDER:" << QTCH_BYTE_ORDER;
    QTCH_LOG_INFO(logger) << "BYTE_ORDER:" << BYTE_ORDER;
    QTCH_LOG_INFO(logger) << "BIG_ENDIAN:" << BIG_ENDIAN;
    QTCH_LOG_INFO(logger) << "LITTLE_ENDIAN:" << LITTLE_ENDIAN;
    QTCH_LOG_INFO(logger) << "begin test_mask";
    qtch::IPv4Address::ptr ipv4 = qtch::IPv4Address::Create("66.5.4.2",80);
    QTCH_LOG_INFO(logger) << "ipv4:" << ipv4;
    QTCH_LOG_INFO(logger) << "ipv4 subnetMask:"<< ipv4->subnetMask(17);
    QTCH_LOG_INFO(logger) << "ipv4 networdAddress:" << ipv4->networkAddress(17);
    QTCH_LOG_INFO(logger) << "ipv4 broadcastAddress:" << ipv4->broadcastAddress(17);

    qtch::IPv6Address::ptr ipv6 = qtch::IPv6Address::Create("234e:0:4567::3d",80);
    QTCH_LOG_INFO(logger) << "ipv6:" << ipv6;
    QTCH_LOG_INFO(logger) << "ipv6 subnetMask:"<<ipv6->subnetMask(17);
    QTCH_LOG_INFO(logger) << "ipv6 networdAddress:" << ipv6->networkAddress(36);
    QTCH_LOG_INFO(logger) << "ipv6 broadcastAddress:" << ipv6->broadcastAddress(36);



    QTCH_LOG_INFO(logger) << "end test_mask";
}

void test_AddressLookup(){
    QTCH_LOG_INFO(logger) << "begin test_AddressLookup";
    std::vector<qtch::Address::ptr> results;
    bool v = qtch::Address::Lookup(results,"www.sylar.top");
    if(!v){
        QTCH_LOG_ERROR(logger) << "lookup faile";
        return;
    }
    for(size_t i=0;i<results.size();++i){
        QTCH_LOG_INFO(logger) << i << " - " << results[i];
    }

    auto addr = qtch::Address::LookupAny("localhost:8009");
    if(addr){
        QTCH_LOG_INFO(logger) << addr;
    }
    else{
        QTCH_LOG_ERROR(logger) << "error";
    }

    QTCH_LOG_INFO(logger) << "end test_AddressLookup";
}

void test_iface(){
    std::multimap<std::string,std::pair<qtch::Address::ptr,uint32_t> > results;
    bool v = qtch::Address::GetInterfaceAddress(results);
    if(!v){
        QTCH_LOG_ERROR(logger) << "GetInterfaceAddress faile";
        return;
    }
    for(auto& i:results){
        QTCH_LOG_INFO(logger) << i.first << " - " << i.second.first << " - " << i.second.second;
    }
}

int main(){
    // test_mask();
    // test_AddressLookup();
    test_iface();
}
