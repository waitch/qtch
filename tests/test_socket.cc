#include "qtch/qtch.h"



static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");

void test_socket(){
    qtch::IPAddress::ptr addr = qtch::Address::LookupAnyIPAdress("www.baidu.com");
    if(addr){
        QTCH_LOG_INFO(logger) << "addr=" << addr << " family=" << addr->getFamily();
    }
    else{
        QTCH_LOG_ERROR(logger) << "lookup Ip address faile";
        return;
    }
    // qtch::IPAddress::ptr addr = qtch::IPv4Address::Create("39.100.72.123",80);
    qtch::Socket::ptr sock= qtch::Socket::CreateTCP(addr);
    addr->setPort(80);
    QTCH_LOG_INFO(logger) << "addr=" << addr;
    QTCH_LOG_INFO(logger) << "begin connect";
    // sleep(10);
    if(!sock->connect(addr)){
        QTCH_LOG_ERROR(logger) << "connect " << addr << " faile";
        return;
    }
    QTCH_LOG_INFO(logger) << "connect " << addr << " connected";
    QTCH_LOG_INFO(logger) << "sock=" << sock;
    const char buffer[] = "GET / HTTP/1.1\r\nHost: www.baidu.com\r\n\r\n";
    int rt = sock->send(buffer,sizeof(buffer));
    if(rt <= 0){
        QTCH_LOG_ERROR(logger) << "send faile rt=" << rt;
        return;
    }
    
    QTCH_LOG_INFO(logger) << "rt=" << rt;
    std::string buf;
    buf.resize(4068);
    sock->setRecvTimeout(5000);
    rt = sock->recv(&buf[0],buf.size());
    if(rt <= 0){
        QTCH_LOG_ERROR(logger) << "recv faile rt=" << rt;
        return;
    }
    
    QTCH_LOG_INFO(logger) <<  "recv over rt=" << rt;
    buf.resize(rt);
    QTCH_LOG_INFO(logger) << buf;
    sock->close();
}

void test_sleep(){
    while(true){
        sleep(1);
        QTCH_LOG_INFO(logger) << "---------------------------";
    }
}


void test_http(){
    qtch::IPAddress::ptr addr = qtch::Address::LookupAnyIPAdress("127.0.0.1");
    if(addr){
        QTCH_LOG_INFO(logger) << "addr=" << addr << " family=" << addr->getFamily();
    }
    else{
        QTCH_LOG_ERROR(logger) << "lookup Ip address faile";
        return;
    }
    // qtch::IPAddress::ptr addr = qtch::IPv4Address::Create("39.100.72.123",80);
    qtch::Socket::ptr sock= qtch::Socket::CreateTCP(addr);
    addr->setPort(8021);
    QTCH_LOG_INFO(logger) << "addr=" << addr;
    QTCH_LOG_INFO(logger) << "begin connect";
    // sleep(10);
    if(!sock->connect(addr)){
        QTCH_LOG_ERROR(logger) << "connect " << addr << " faile";
        return;
    }
    QTCH_LOG_INFO(logger) << "connect " << addr << " connected";
    QTCH_LOG_INFO(logger) << "sock=" << sock;
    // const char buffer1[] = "GE";
    // const char buffer2[] = "T / HTTP/1.1\r\nHost: www.baidu.com\r\n\r\n";
    const char buffer1[] = "GET / HTTP/1.1\r\nHost: www.baidu.com\r\n";
    const char buffer2[] = "XXX: acb\r\n\r\n";
    int rt = sock->send(buffer1,sizeof(buffer1)-1);
    if(rt <= 0){
        QTCH_LOG_ERROR(logger) << "send buffer1 faile rt=" << rt;
        return;
    }
    sleep(1);
    rt = sock->send(buffer2,sizeof(buffer2)-1);
    if(rt <= 0){
        QTCH_LOG_ERROR(logger) << "send buffer2 faile rt=" << rt;
        return;
    }
    
    QTCH_LOG_INFO(logger) << "rt=" << rt;
    std::string buf;
    buf.resize(4068);
    sock->setRecvTimeout(5000);
    rt = sock->recv(&buf[0],buf.size());
    if(rt <= 0){
        QTCH_LOG_ERROR(logger) << "recv faile rt=" << rt;
        return;
    }
    
    QTCH_LOG_INFO(logger) <<  "recv over rt=" << rt;
    buf.resize(rt);
    QTCH_LOG_INFO(logger) << buf;
    sock->close();
}


int main(){
    qtch::IOManager iom(2,false,"test_socket");
    // iom.schedule(test_socket);
    // iom.schedule(test_sleep);
    iom.schedule(test_http);
    iom.start();
    iom.stop();
}