#include "qtch/qtch.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");




int main(int argc,char** argv){
    QTCH_LOG_INFO(logger) << "argc=" << argc;
    qtch::EnvMgr::GetInstance()->addHelp("p","print help");
    qtch::EnvMgr::GetInstance()->addHelp("s","start with terminal");
    qtch::EnvMgr::GetInstance()->addHelp("d","start witch daemon");
    if(!qtch::EnvMgr::GetInstance()->init(argc,argv)){
        qtch::EnvMgr::GetInstance()->printfHelp();
        return 0;
    }
    QTCH_LOG_INFO(logger) << "exe=" << qtch::EnvMgr::GetInstance()->getExe();
    QTCH_LOG_INFO(logger) << "cwd=" << qtch::EnvMgr::GetInstance()->getCwd();
    QTCH_LOG_INFO(logger) << "conf=" << qtch::EnvMgr::GetInstance()->getConfigPath();
    QTCH_LOG_INFO(logger) << "program=" << qtch::EnvMgr::GetInstance()->getProgram();

    QTCH_LOG_INFO(logger) << "path=" << qtch::EnvMgr::GetInstance()->getEnv("PATH");

    if(qtch::EnvMgr::GetInstance()->has("p")){
        qtch::EnvMgr::GetInstance()->printfHelp();
    }

}