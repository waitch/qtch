#include "qtch/qtch.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");

const char test_request_data[] = "POST / HTTP/1.1\r\n"
                                "Host: www.sylar.top\r\n"
                                "Content-Length: 0\r\n\r\n"
                                "1234567890";


void test_request(){
    QTCH_LOG_INFO(logger) << "test request";
    qtch::http::HttpRequestParser parser;
    std::string tmp = std::string(test_request_data);
    size_t s = parser.execute(&tmp[0],tmp.size());
    QTCH_LOG_INFO(logger) << "execute rt=" << s
                    << " has_error=" << parser.hasError()
                    << " is_finished=" << parser.isFinished()
                    << " total=" << tmp.size()
                    << " content_length=" << parser.getContentLength();
    tmp.resize(tmp.size() - s);
    QTCH_LOG_INFO(logger) << parser.getData()->toString();
    QTCH_LOG_INFO(logger) << tmp;
}

const char test_response_data[] = "HTTP/1.1 200 OK\r\n"
        "Date: Tue, 04 Jun 2019 15:43:56 GMT\r\n"
        "Server: Apache\r\n"
        "Last-Modified: Tue, 12 Jan 2010 13:48:00 GMT\r\n"
        "ETag: \"51-47cf7e6ee8400\"\r\n"
        "Accept-Ranges: bytes\r\n"
        "Content-Length: 81\r\n"
        "Cache-Control: max-age=86400\r\n"
        "Expires: Wed, 05 Jun 2019 15:43:56 GMT\r\n"
        "Connection: Close\r\n"
        "Content-Type: text/html\r\n"
        "\r\n"
        "<html>\r\n"
        "<meta http-equiv=\"refresh\" content=\"0;url=http://www.baidu.com/\">\r\n"
        "</html>\r\n";





void test_response(){
    QTCH_LOG_INFO(logger) << "test response";
    qtch::http::HttpResponseParser parser;
    std::string tmp = std::string(test_response_data);
    size_t s = parser.execute(&tmp[0],tmp.size(),false);
    QTCH_LOG_INFO(logger) << "execute rt=" << s
                    << " has_error=" << parser.hasError()
                    << " is_finished=" << parser.isFinished()
                    << " total=" << tmp.size()
                    << " content_length=" << parser.getContentLength()
                    << " tmp[s]:" << tmp[0];
    tmp.resize(tmp.size() - s);

    QTCH_LOG_INFO(logger) << parser.getData()->toString();
    QTCH_LOG_INFO(logger) << tmp;
    QTCH_LOG_INFO(logger) << tmp.size();
}

int main(){
    test_request();
    test_response();
}

