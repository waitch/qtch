#include "qtch/qtch.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");

void test_httpPool(){
    QTCH_LOG_DEBUG(logger) << "test_httpPool";
    qtch::http::HttpConnectionPool::ptr pool = std::make_shared<qtch::http::HttpConnectionPool>("www.sylar.top","",80,false,10,1000*30,5);
    qtch::IOManager::getThis()->addTimer(1000,[pool](){
        qtch::http::HttpResult::ptr re = pool->DoGet("/",300);
        QTCH_LOG_INFO(logger) << re->toString();
    },true);
}

int main(){
    qtch::IOManager iom(2);
    iom.schedule(test_httpPool);
    iom.start();
}