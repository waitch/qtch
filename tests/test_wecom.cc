#include"qtch/qtch.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");

void test_wecom_send_text_message(){
    
    qtch::infoPush::WeComPusherBuild::ptr pusherBuild 
                    = qtch::infoPush::WeComPusherBuild::build("lili");
    bool rt = pusherBuild->addUser("QiuJianLi")
                        ->sendTextMessage("hello world!")
                        ->send();
    if(rt){
        QTCH_LOG_INFO(logger) << "send success";
    }else {
        QTCH_LOG_INFO(logger) << "send faile errorCode:" << pusherBuild->getErrorCode()
                    << " errorMessage:" << pusherBuild->getErrorMessage();
    }



}

int main(){
    test_wecom_send_text_message();
}