#include "qtch/util.h"
#include "qtch/log.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");

void test_base64(){
    std::string origin = "904389094@qq.com";
    QTCH_LOG_INFO(logger) << "origin:" << origin << " size=" << origin.size() << " length=" << origin.length();
    std::string after = qtch::CypherUtil::encodeBase64(origin);
    QTCH_LOG_INFO(logger) << "after:" << after;
    QTCH_LOG_INFO(logger) << "origin:" << qtch::CypherUtil::decodeBase64(after);
}


int main(){
    test_base64();
}