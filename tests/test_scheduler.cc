#include "qtch/qtch.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");

void test_fiber(){
    static int s_count = 6;
    QTCH_LOG_INFO(logger) << "test in fiber s_count=" << s_count;

    sleep(1);
    if(--s_count >= 0) {
        QTCH_LOG_INFO(logger) << "----------------------";
        qtch::Scheduler::getThis()->schedule(&test_fiber, -1);
    }
}


void test_scheduler(){
    QTCH_LOG_INFO(logger) <<"begin test scheduler";
    qtch::Scheduler::ptr sc(new qtch::Scheduler(4,false,"test"));
    QTCH_LOG_INFO(logger) <<"schedule";
    sc->schedule(&test_fiber);
    sc->start();
    sleep(10);
    sc->stop();
    QTCH_LOG_INFO(logger) <<"active thread count:" << sc->getActiveThreadCount() 
                    << " idle thread count:" << sc->getIdleThreadCount() ;
    QTCH_LOG_INFO(logger) <<"end test scheduler";

}

void test_show_Fiber_count(){
    QTCH_LOG_INFO(logger) <<"fiber count:" << qtch::Fiber::getFibersCount();
}

void test(){
    test_scheduler();
    test_show_Fiber_count();
}

class Test:public std::enable_shared_from_this<Test>{
public:
    typedef std::shared_ptr<Test> ptr;
    Test(){
        QTCH_LOG_INFO(logger) <<"Test create";
    }
    ~Test(){
        QTCH_LOG_INFO(logger) <<"Test destory";
    }
};

void test_share_ptr(){
    Test::ptr x(new Test);
    QTCH_LOG_INFO(logger) <<"fiber count:" << x.use_count();
    auto y = x;
    QTCH_LOG_INFO(logger) <<"fiber count:" << x.use_count();
    y = nullptr;
    QTCH_LOG_INFO(logger) <<"fiber count:" << x.use_count();
    x=x;
    QTCH_LOG_INFO(logger) <<"fiber count:" << x.use_count();
}

int main(){
    // qtch::Config::LoadConfigDir("/home/qiu/qtch/bin/conf");
    test();
    // test_share_ptr();
}