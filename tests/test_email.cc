#include "qtch/email/smtp.h"
#include "qtch/log.h"
#include "qtch/config.h"


static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");

static qtch::ConfigVar<std::string>::ptr fromAddress = qtch::Config::LookUp<std::string>("email.address","","from address");
static qtch::ConfigVar<std::string>::ptr fromAddressPwd = qtch::Config::LookUp<std::string>("email.pwd","","address pwd");

void test_send_email(){
    std::vector<std::string> toAddress;
    toAddress.push_back("qiujianli98@126.com");
    qtch::EMail::ptr email = qtch::EMail::Create(fromAddress->getValue(),fromAddressPwd->getValue(),"测试","hello email",toAddress);
    qtch::SmtpClient::ptr smtpClient = qtch::SmtpClient::Create("smtp.qq.com",465,true);
    qtch::SmtpResult::ptr result = smtpClient->send(email,1000,true);
    if(result->result != qtch::SmtpResult::Result::OK){
        QTCH_LOG_ERROR(logger) << "send email fail, errmsg=" << result->msg
            << "debug:\n"  << smtpClient->getDebugInfo();
        return;
    }
    QTCH_LOG_INFO(logger) << "send email success";
    QTCH_LOG_INFO(logger) << "debug:\n" << smtpClient->getDebugInfo();

    
}

int main(){
    test_send_email();
}