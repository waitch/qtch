#include "qtch/util.h"
#include "qtch/log.h"
#include "qtch/macro.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");

void test_base64(){
    const std::string a= "hello world!";
    QTCH_LOG_DEBUG(logger) << "Source string=" << a;
    std::string base64 = qtch::CypherUtil::encodeBase64(a);
    QTCH_LOG_DEBUG(logger) << "after base64=" << base64;
    std::string deBase64 = qtch::CypherUtil::decodeBase64(base64);
    QTCH_LOG_DEBUG(logger) << "decodeBase64=" << deBase64;

    QTCH_ASSERT2(a==deBase64," decodeBase64 Error!");
}

void test_md5(){
    const std::string a = "hello world!";
    std::string a_md5 = "FC3FF98E8C6A0D3087D515C0473F8677";
    std::string result = qtch::HashUtil::md5Sum(a);
    QTCH_LOG_DEBUG(logger) << "a=" << a;
    QTCH_LOG_DEBUG(logger) << "after md5 a=" << result;
    QTCH_LOG_DEBUG(logger) << "correct md5 a=" << a_md5;
    QTCH_ASSERT2(result==a_md5," md5 Error!");
}

void test1(std::string &a){
    a = "acbsdadasdasdxcdww";
}

void test2(){
    std::string a = "bca";
    test1(a);
    QTCH_LOG_INFO(logger) << a;
}

int main(){
    test_base64();
    test_md5();
    test2();
}