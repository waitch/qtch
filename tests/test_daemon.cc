#include "qtch/qtch.h"

static qtch::Logger::ptr logger = QTCH_LOG_NAME("test");


static qtch::Timer::ptr times;
int test_daemon(int argc,char** argv){
    QTCH_LOG_INFO(logger) << qtch::ProcessInfoMgr::GetInstance()->toString();
    qtch::IOManager iom(1);
    times = iom.addTimer(1000,[](){
        QTCH_LOG_INFO(logger) << "onTimer";
        static int count = 0;
        if(count++>10){
            times->cancel();
        }
    },true);
    iom.start();
    return 0;
}

int main(int argc,char** argv){
    return qtch::start_daemon(argc,argv,test_daemon,argc!=1);
}